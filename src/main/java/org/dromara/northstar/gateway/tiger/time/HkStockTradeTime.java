package org.dromara.northstar.gateway.tiger.time;

import org.dromara.northstar.common.model.core.TradeTimeDefinition;

import java.time.LocalTime;
import java.util.List;


/**
 * 港股连续交易时段 
 * @author KevinHuangwl
 *
 */
/*
public class HkStockTradeTime implements TradeTimeDefinition {

	@Override
	public List<PeriodSegment> tradeTimeSegments() {
		return List.of(
				new PeriodSegment(LocalTime.of(10, 1), LocalTime.of(12, 0)),
				new PeriodSegment(LocalTime.of(13, 1), LocalTime.of(16, 0)));
	}

}
*/
