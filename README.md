# northstar-gateway-tiger

northstar盈富量化平台老虎证券网关接口实现

使用时，需要依赖 northstar 主程序进行加载，详情参考 https://gitee.com/dromara/northstar
由于老虎证券对查询合约接口做了速率限制，增加了合约缓存json文件，自行拷贝到主项目的根目录下
[contracts_cache_CN.json](contracts_cache_CN.json)
[contracts_cache_HK.json](contracts_cache_HK.json)
[contracts_cache_US.json](contracts_cache_US.json)
